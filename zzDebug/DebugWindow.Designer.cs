﻿namespace zzDebug
{
    partial class DebugWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.xPosPlayer = new System.Windows.Forms.TextBox();
            this.yPosPlayer = new System.Windows.Forms.TextBox();
            this.zPosPlayer = new System.Windows.Forms.TextBox();
            this.debugTimer = new System.Windows.Forms.Timer(this.components);
            this.zPosTarget = new System.Windows.Forms.TextBox();
            this.yPosTarget = new System.Windows.Forms.TextBox();
            this.xPosTarget = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.npcFlagsTarget = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.GameObjectList = new System.Windows.Forms.TextBox();
            this.GameObjectTimer = new System.Windows.Forms.Timer(this.components);
            this.flagDebug = new System.Windows.Forms.TextBox();
            this.debugTargetGuid = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.debugTargetId = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.debugTargetBuffs = new System.Windows.Forms.TextBox();
            this.debugTargetDebuffs = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.debugObjectList = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.WoWGameObjectToggle = new System.Windows.Forms.Button();
            this.WoWObjectToggle = new System.Windows.Forms.Button();
            this.playerIsDead = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.debugPlayerGhost = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // xPosPlayer
            // 
            this.xPosPlayer.Location = new System.Drawing.Point(12, 12);
            this.xPosPlayer.Name = "xPosPlayer";
            this.xPosPlayer.Size = new System.Drawing.Size(150, 20);
            this.xPosPlayer.TabIndex = 0;
            // 
            // yPosPlayer
            // 
            this.yPosPlayer.Location = new System.Drawing.Point(12, 38);
            this.yPosPlayer.Name = "yPosPlayer";
            this.yPosPlayer.Size = new System.Drawing.Size(150, 20);
            this.yPosPlayer.TabIndex = 1;
            // 
            // zPosPlayer
            // 
            this.zPosPlayer.Location = new System.Drawing.Point(12, 64);
            this.zPosPlayer.Name = "zPosPlayer";
            this.zPosPlayer.Size = new System.Drawing.Size(150, 20);
            this.zPosPlayer.TabIndex = 2;
            // 
            // debugTimer
            // 
            this.debugTimer.Enabled = true;
            this.debugTimer.Interval = 10;
            this.debugTimer.Tick += new System.EventHandler(this.debugTimer_Tick);
            // 
            // zPosTarget
            // 
            this.zPosTarget.Location = new System.Drawing.Point(186, 64);
            this.zPosTarget.Name = "zPosTarget";
            this.zPosTarget.Size = new System.Drawing.Size(150, 20);
            this.zPosTarget.TabIndex = 5;
            // 
            // yPosTarget
            // 
            this.yPosTarget.Location = new System.Drawing.Point(186, 38);
            this.yPosTarget.Name = "yPosTarget";
            this.yPosTarget.Size = new System.Drawing.Size(150, 20);
            this.yPosTarget.TabIndex = 4;
            // 
            // xPosTarget
            // 
            this.xPosTarget.Location = new System.Drawing.Point(186, 12);
            this.xPosTarget.Name = "xPosTarget";
            this.xPosTarget.Size = new System.Drawing.Size(150, 20);
            this.xPosTarget.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(168, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "x";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(168, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(12, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "y";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(168, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(12, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "z";
            // 
            // npcFlagsTarget
            // 
            this.npcFlagsTarget.Location = new System.Drawing.Point(186, 90);
            this.npcFlagsTarget.Name = "npcFlagsTarget";
            this.npcFlagsTarget.Size = new System.Drawing.Size(150, 20);
            this.npcFlagsTarget.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(128, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "NpcFlags";
            // 
            // GameObjectList
            // 
            this.GameObjectList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.GameObjectList.Location = new System.Drawing.Point(649, 38);
            this.GameObjectList.Multiline = true;
            this.GameObjectList.Name = "GameObjectList";
            this.GameObjectList.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.GameObjectList.Size = new System.Drawing.Size(305, 530);
            this.GameObjectList.TabIndex = 11;
            this.GameObjectList.WordWrap = false;
            // 
            // GameObjectTimer
            // 
            this.GameObjectTimer.Enabled = true;
            this.GameObjectTimer.Interval = 400;
            this.GameObjectTimer.Tick += new System.EventHandler(this.GameObjectTimer_Tick);
            // 
            // flagDebug
            // 
            this.flagDebug.Location = new System.Drawing.Point(342, 38);
            this.flagDebug.Multiline = true;
            this.flagDebug.Name = "flagDebug";
            this.flagDebug.Size = new System.Drawing.Size(301, 72);
            this.flagDebug.TabIndex = 12;
            // 
            // debugTargetGuid
            // 
            this.debugTargetGuid.Location = new System.Drawing.Point(526, 12);
            this.debugTargetGuid.Name = "debugTargetGuid";
            this.debugTargetGuid.Size = new System.Drawing.Size(117, 20);
            this.debugTargetGuid.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(488, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Guid:";
            // 
            // debugTargetId
            // 
            this.debugTargetId.Location = new System.Drawing.Point(369, 12);
            this.debugTargetId.Name = "debugTargetId";
            this.debugTargetId.Size = new System.Drawing.Size(113, 20);
            this.debugTargetId.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(342, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "ID:";
            // 
            // debugTargetBuffs
            // 
            this.debugTargetBuffs.Location = new System.Drawing.Point(83, 164);
            this.debugTargetBuffs.Name = "debugTargetBuffs";
            this.debugTargetBuffs.Size = new System.Drawing.Size(253, 20);
            this.debugTargetBuffs.TabIndex = 17;
            // 
            // debugTargetDebuffs
            // 
            this.debugTargetDebuffs.Location = new System.Drawing.Point(83, 190);
            this.debugTargetDebuffs.Name = "debugTargetDebuffs";
            this.debugTargetDebuffs.Size = new System.Drawing.Size(253, 20);
            this.debugTargetDebuffs.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 167);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Target Buffs";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(33, 193);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "Debuffs";
            // 
            // debugObjectList
            // 
            this.debugObjectList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.debugObjectList.Location = new System.Drawing.Point(960, 38);
            this.debugObjectList.Multiline = true;
            this.debugObjectList.Name = "debugObjectList";
            this.debugObjectList.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.debugObjectList.Size = new System.Drawing.Size(321, 530);
            this.debugObjectList.TabIndex = 21;
            this.debugObjectList.WordWrap = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(649, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "WoWGameObject";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(957, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "WoWObject";
            // 
            // WoWGameObjectToggle
            // 
            this.WoWGameObjectToggle.Location = new System.Drawing.Point(750, 11);
            this.WoWGameObjectToggle.Name = "WoWGameObjectToggle";
            this.WoWGameObjectToggle.Size = new System.Drawing.Size(103, 23);
            this.WoWGameObjectToggle.TabIndex = 24;
            this.WoWGameObjectToggle.Text = "Toggle Update";
            this.WoWGameObjectToggle.UseVisualStyleBackColor = true;
            this.WoWGameObjectToggle.Click += new System.EventHandler(this.WoWGameObjectToggle_Click);
            // 
            // WoWObjectToggle
            // 
            this.WoWObjectToggle.Location = new System.Drawing.Point(1029, 12);
            this.WoWObjectToggle.Name = "WoWObjectToggle";
            this.WoWObjectToggle.Size = new System.Drawing.Size(103, 23);
            this.WoWObjectToggle.TabIndex = 25;
            this.WoWObjectToggle.Text = "Toggle Update";
            this.WoWObjectToggle.UseVisualStyleBackColor = true;
            this.WoWObjectToggle.Click += new System.EventHandler(this.WoWObjectToggle_Click);
            // 
            // playerIsDead
            // 
            this.playerIsDead.Location = new System.Drawing.Point(83, 90);
            this.playerIsDead.Name = "playerIsDead";
            this.playerIsDead.Size = new System.Drawing.Size(39, 20);
            this.playerIsDead.TabIndex = 26;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 93);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 13);
            this.label11.TabIndex = 27;
            this.label11.Text = "Player Alive:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 116);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 13);
            this.label12.TabIndex = 29;
            this.label12.Text = "Player Ghost:";
            // 
            // debugPlayerGhost
            // 
            this.debugPlayerGhost.Location = new System.Drawing.Point(83, 113);
            this.debugPlayerGhost.Name = "debugPlayerGhost";
            this.debugPlayerGhost.Size = new System.Drawing.Size(39, 20);
            this.debugPlayerGhost.TabIndex = 28;
            // 
            // DebugWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1290, 575);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.debugPlayerGhost);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.playerIsDead);
            this.Controls.Add(this.WoWObjectToggle);
            this.Controls.Add(this.WoWGameObjectToggle);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.debugObjectList);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.debugTargetDebuffs);
            this.Controls.Add(this.debugTargetBuffs);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.debugTargetId);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.debugTargetGuid);
            this.Controls.Add(this.flagDebug);
            this.Controls.Add(this.GameObjectList);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.npcFlagsTarget);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.zPosTarget);
            this.Controls.Add(this.yPosTarget);
            this.Controls.Add(this.xPosTarget);
            this.Controls.Add(this.zPosPlayer);
            this.Controls.Add(this.yPosPlayer);
            this.Controls.Add(this.xPosPlayer);
            this.Name = "DebugWindow";
            this.Text = "DebugWindow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox xPosPlayer;
        private System.Windows.Forms.TextBox yPosPlayer;
        private System.Windows.Forms.TextBox zPosPlayer;
        private System.Windows.Forms.Timer debugTimer;
        private System.Windows.Forms.TextBox zPosTarget;
        private System.Windows.Forms.TextBox yPosTarget;
        private System.Windows.Forms.TextBox xPosTarget;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox npcFlagsTarget;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox GameObjectList;
        private System.Windows.Forms.Timer GameObjectTimer;
        private System.Windows.Forms.TextBox flagDebug;
        private System.Windows.Forms.TextBox debugTargetGuid;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox debugTargetId;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox debugTargetBuffs;
        private System.Windows.Forms.TextBox debugTargetDebuffs;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox debugObjectList;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button WoWGameObjectToggle;
        private System.Windows.Forms.Button WoWObjectToggle;
        private System.Windows.Forms.TextBox playerIsDead;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox debugPlayerGhost;
    }
}