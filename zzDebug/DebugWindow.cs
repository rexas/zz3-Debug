﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZzukBot.Game.Statics;
using ZzukBot.Objects;

namespace zzDebug
{
    public partial class DebugWindow : Form
    {
        public static LocalPlayer Player { get { return ObjectManager.Instance.Player; } }
        public static WoWUnit Target { get { return ObjectManager.Instance.Target; } }
        public DebugWindow()
        {
            InitializeComponent();
        }

        private void debugTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (Player != null)
                {
                    xPosPlayer.Text = Player.Position.X.ToString();
                    yPosPlayer.Text = Player.Position.Y.ToString();
                    zPosPlayer.Text = Player.Position.Z.ToString();
                    playerIsDead.Text = (Player.IsDead == false).ToString();
                    debugPlayerGhost.Text = (Player.InGhostForm).ToString();
                }
                if (Target != null)
                {
                    xPosTarget.Text = Target.Position.X.ToString();
                    yPosTarget.Text = Target.Position.Y.ToString();
                    zPosTarget.Text = Target.Position.Z.ToString();
                    debugTargetId.Text = Target.Id.ToString();
                    debugTargetGuid.Text = Target.Guid.ToString();
                    npcFlagsTarget.Text = ((int)Target.NpcFlags).ToString();

                    string tmp = Target.NpcFlags.ToString() + "\r\n";
                    IEnumerable<NPCFlags> flags = Enum.GetValues(typeof(NPCFlags)).Cast<NPCFlags>();
                    foreach(NPCFlags flag in flags)
                    {
                        if (((int)Target.NpcFlags & (int)flag) == (int)flag)
                            tmp += flag.ToString() + ": TRUE\r\n";
                    }
                    flagDebug.Text = tmp;

                    debugTargetBuffs.Text = string.Join(",", Target.Auras);
                    debugTargetDebuffs.Text = string.Join(",", Target.Debuffs);


                }
            }catch(Exception ex) { }
        }

        public void Destroy()
        {
            debugTimer.Enabled = false;
        }

        private void GameObjectTimer_Tick(object sender, EventArgs e)
        {
            string tmp = "";

            if(_WoWGameObjectUpdate)
            {
                foreach (WoWGameObject go in ObjectManager.Instance.GameObjects)
                {
                    tmp = tmp + go?.Id + " \t" + go?.Name + "\r\n" + go?.Position + "\r\n";
                }
                GameObjectList.Text = tmp;
            }


            if(_WoWObjectUpdate)
            {
                tmp = "";
                foreach (WoWObject obj in ObjectManager.Instance.Objects)
                {
                    tmp = tmp + obj?.WoWType + " \t" + obj?.Name + "\r\n" + obj?.Position + "\r\n";
                }
                debugObjectList.Text = tmp;
            }
        }

        public enum NPCFlags
        {
            UNIT_NPC_FLAG_NONE = 0x00000000,
            UNIT_NPC_FLAG_GOSSIP = 0x00000001,       ///< 100%
            UNIT_NPC_FLAG_QUESTGIVER = 0x00000002,       ///< guessed, probably ok
            UNIT_NPC_FLAG_VENDOR = 0x00000004,       ///< 100%
            UNIT_NPC_FLAG_FLIGHTMASTER = 0x00000008,       ///< 100%
            UNIT_NPC_FLAG_TRAINER = 0x00000010,       ///< 100%
            UNIT_NPC_FLAG_SPIRITHEALER = 0x00000020,       ///< guessed
            UNIT_NPC_FLAG_SPIRITGUIDE = 0x00000040,       ///< guessed
            UNIT_NPC_FLAG_INNKEEPER = 0x00000080,       ///< 100%
            UNIT_NPC_FLAG_BANKER = 0x00000100,       ///< 100%
            UNIT_NPC_FLAG_PETITIONER = 0x00000200,       ///< 100% 0xC0000 = guild petitions
            UNIT_NPC_FLAG_TABARDDESIGNER = 0x00000400,       ///< 100%
            UNIT_NPC_FLAG_BATTLEMASTER = 0x00000800,       ///< 100%
            UNIT_NPC_FLAG_AUCTIONEER = 0x00001000,       ///< 100%
            UNIT_NPC_FLAG_STABLEMASTER = 0x00002000,       ///< 100%
            UNIT_NPC_FLAG_REPAIR = 0x00004000,       ///< 100%
            UNIT_NPC_FLAG_OUTDOORPVP = 0x20000000        ///< custom flag for outdoor pvp creatures || Custom flag
        };

        bool _WoWGameObjectUpdate = true;
        private void WoWGameObjectToggle_Click(object sender, EventArgs e)
        {
            _WoWGameObjectUpdate = !_WoWGameObjectUpdate;
        }

        bool _WoWObjectUpdate = true;
        private void WoWObjectToggle_Click(object sender, EventArgs e)
        {
            _WoWObjectUpdate = !_WoWObjectUpdate;
        }
    }
}
