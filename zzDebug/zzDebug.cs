﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using System.Windows.Forms;
using ZzukBot;
using ZzukBot.ExtensionFramework.Interfaces;
using System.Runtime.InteropServices;
using System.IO;
using System.Media;

namespace zzDebug
{
    [Export(typeof(IPlugin))]
    public class zzDebug : IPlugin
    {
        private DebugWindow _dw;
        public string Author
        {
            get
            {
                return "Rexas";
            }
        }

        public string Name
        {
            get
            {
                return "zzDebug";
            }
        }

        public Version Version
        {
            get
            {
                return new Version(0,1);
            }
        }

        public void Dispose()
        {
            if (_dw != null)
            {
                _dw.Close();
                _dw.Dispose();
            }
            _dw = null;
        }

        public bool Load()
        {
            _dw  = new DebugWindow();
            _dw.Show();
            return true;
        }

        public void ShowGui()
        {
            if(_dw == null)
            {
                _dw = new DebugWindow();
            }
            try
            {
                _dw.Show();
            }
            catch(Exception ex) { }
        }

        public void Unload()
        {
            if(_dw != null)
            {
                _dw.Close();
                _dw.Dispose();
            }
            _dw = null;
        }
    }
}
